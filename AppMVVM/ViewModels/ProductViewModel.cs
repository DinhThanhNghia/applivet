﻿using AppMVVM.Models;
using AppMVVM.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace AppMVVM.ViewModels
{
    public class ProductViewModel : BaseViewModel
    {
        private readonly ProductService _productService;
        private readonly OrderService _orderService;

        #region Initial Paramanter to binding
        private List<Product> _List;
        public List<Product> List { get => _List; set { _List = value; OnPropertyChanged(); } }

        private Product _SelectedItem;
        public Product SelectedItem
        {
            get => _SelectedItem;
            set
            {
                _SelectedItem = value;
                OnPropertyChanged();
                if (SelectedItem != null)
                {
                    Id = SelectedItem.Id.ToString();
                    Name = SelectedItem.Name;
                    Detail = SelectedItem.Detail;
                    Status = SelectedItem.Status;
                    Unit = SelectedItem.Unit;
                    Price = SelectedItem.Price;
                }
            }
        }

        private string _Id;
        public string Id { get => _Id; set { _Id = value; OnPropertyChanged(); } }

        private string _Name;
        public string Name { get => _Name; set { _Name = value; OnPropertyChanged(); } }

        private string _Detail;
        public string Detail { get => _Detail; set { _Detail = value; OnPropertyChanged(); } }

        private string _Status;
        public string Status { get => _Status; set { _Status = value; OnPropertyChanged(); } }

        private string _Unit;
        public string Unit { get => _Unit; set { _Unit = value; OnPropertyChanged(); } }

        private double? _Price;
        public double? Price { get => _Price; set { _Price = value; OnPropertyChanged(); } }

        private DateTime _CreatedDate;
        public DateTime CreatedDate { get => _CreatedDate; set { _CreatedDate = value; OnPropertyChanged(); } }

        private string _PrevButton;
        public string PrevButton { get => _PrevButton; set { _PrevButton = value; OnPropertyChanged(); } }

        private string _NextButton;
        public string NextButton { get => _NextButton; set { _NextButton = value; OnPropertyChanged(); } }

        private int _pageIndex;
        public int pageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                if (_pageIndex != value)
                {
                    _pageIndex = value;
                    if (_pageIndex == 1)
                    {
                        PrevButton = "False";
                    }
                    else
                    {
                        PrevButton = "True";
                    }
                    OnPropertyChanged("pageIndex");
                }
            }
        }

        private readonly int pageSize;
        public int productCount;
        #endregion

        #region Initial Button Command binding event
        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        #endregion

        #region Constructor
        //Constructor - Xử lý tất cả event trong này!!!
        public ProductViewModel()
        {
            _productService = new ProductService();
            _orderService = new OrderService();

            PrevButton = "False";
            NextButton = "True";
            pageIndex = 1;
            pageSize = 10;

            productCount = _productService.GetProducts().Count;
            GetProducts();

            AddCommand = new RelayCommand<object>((p) =>
            {
                return true;
            }, (p) =>
            {
                AddProduct();
                productCount = _productService.GetProducts().Count;
                GetProducts();
            });

            EditCommand = new RelayCommand<object>((p) =>
            {
                if (SelectedItem == null)
                    return false;
                return true;
            }, (p) =>
            {
                EditProduct();
                GetProducts();
            });

            DeleteCommand = new RelayCommand<object>((p) =>
            {
                if (SelectedItem == null)
                    return false;
                return true;
            }, (p) =>
            {
                DeleteProduct(SelectedItem.Id);
                //List = new List<Product>(GetProducts());
                productCount = _productService.GetProducts().Count;
                GetProducts();
            });
        }
        #endregion

        #region CRUD event method
        //Method get list product
        private void GetProducts()
        {
            var products = _productService.GetAllPaging(pageSize, pageIndex);
            if (pageSize * pageIndex >= productCount)
            {
                NextButton = "False";
            }
            else
            {
                NextButton = "True";
            }
            List = products;
        }

        //Add new product
        private void AddProduct()
        {
            var products = new Product()
            {
                Name = Name,
                Detail = Detail,
                Unit = Unit,
                Status = Status,
                Price = Price
            };

            //Validate data
            string returnMessage = "Validation Message:";
            bool returnFlag = false;

            if (string.IsNullOrEmpty(Name))
            {
                returnMessage += Environment.NewLine + "- Product's name must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Detail))
            {
                returnMessage += Environment.NewLine + "- Detail must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Unit))
            {
                returnMessage += Environment.NewLine + "- Unit must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Price.ToString()))
            {
                returnMessage += Environment.NewLine + "- Price must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Status))
            {
                returnMessage += Environment.NewLine + "- Status must not be empty.";
                returnFlag = true;
            }

            if (returnFlag)
            {
                MessageBox.Show(returnMessage);
            }
            else
            {
                // Function call the API to add Product and get response data.
                var response = _productService.AddProduct(products);
                var responseData = response.Content.ReadAsStringAsync().Result;
            }
            
        }

        //Edit product
        private void EditProduct()
        {
            var products = new Product()
            {
                Id = int.Parse(Id),
                Name = Name,
                Detail = Detail,
                Unit = Unit,
                Status = Status,
                Price = Price,
            };
            // Function call the API to add Product and get response data.
            if (products.Id == SelectedItem.Id)
            {
                var response = _productService.EditProduct(products);
                var responseData = response.Content.ReadAsStringAsync().Result;
            }
        }

        //Delete product => IsDelete = 1
        private void DeleteProduct(int id)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Delete this Product?", "Delete Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                List<Order> orders = _orderService.ListOrder();
                var result = orders.Where(x => x.Product.Id == id).Count();
                if (result > 0)
                {
                    MessageBox.Show("This record was used in Order table.");
                }
                else
                {
                    HttpResponseMessage response = _productService.DeleteProduct(id);
                }
            }
        }
        #endregion

        #region Extension method for event trigger
        //Validate input isnumber
        public void IsAllowedInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        //Method pagination
        public void PreviousPage()
        {
            pageIndex--;
            GetProducts();
        }
        public void NextPage()
        {
            pageIndex++;
            GetProducts();
        }
        #endregion
    }
}