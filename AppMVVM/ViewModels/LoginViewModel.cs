﻿using AppMVVM.Models;
using AppMVVM.Services;
using AppMVVM.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AppMVVM.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly UserService _userService;

        public bool IsLogin { get; set; }
        private string _UserName;
        public string UserName { get => _UserName; set { _UserName = value; OnPropertyChanged(); } }
        private string _Password;
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }

        public ICommand LoginCommand { get; set; }
        public ICommand PasswordChangedCommand { get; set; }

        public LoginViewModel()
        {
            IsLogin = false;
            Password = "";
            UserName = "";
            _userService = new UserService();

            LoginCommand = new RelayCommand<Window>((p) => 
            {
                return true;
            }, (p) => 
            {
                // Close Login Window
                Login(p);
            });

            PasswordChangedCommand = new RelayCommand<PasswordBox>((p) => 
            {
                return true;
            }, (p) => 
            {
                Password = p.Password;
            });
        }

        private void Login(Window p)
        {
            // Completing the URL and call the API to get response data.
            HttpResponseMessage response = _userService.Login(UserName, Password);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (p == null)
                return;
            if (UserName == "")
            {
                IsLogin = false;
                MessageBox.Show("User name has been required");
            }
            else if (Password == "")
            {
                IsLogin = false;
                MessageBox.Show("Password has been required");
            }
            else if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data.
                var data = JsonConvert.DeserializeObject<User>(responseData);

                // Set token data and username to App.Current.Properties. These work like global variables.
                Application.Current.Properties["userId"] = data.Id;
                Application.Current.Properties["username"] = UserName;

                IsLogin = true;
                p.Close();
            }
            else
            {
                IsLogin = false;
                MessageBox.Show("User name or password was found. Please check again!");
            }
        }
    }
}
