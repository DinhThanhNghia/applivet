﻿using AppMVVM.Models;
using AppMVVM.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace AppMVVM.ViewModels
{
    public class OrderViewModel : BaseViewModel
    {
        private readonly OrderService _orderService;
        private readonly ProductService _productService;

        #region Initial paramanter to binding
        //Initial OrderList to add new order to database
        public ObservableCollection<OrderModel> _OrderList;
        public ObservableCollection<OrderModel> OrderList { get => _OrderList; set { _OrderList = value; OnPropertyChanged(); } }

        private OrderModel _SelectedItem;
        public OrderModel SelectedItem { get => _SelectedItem; set { _SelectedItem = value; OnPropertyChanged(); } }

        //Initial ProductList to select product to orderList
        public static List<Product> _ProductList;
        public List<Product> ProductList
        {
            get => _ProductList;
            set
            {
                _ProductList = value;
                OnPropertyChanged();
            }
        }

        //Initial HistoryOrderList to show history Order
        public List<OrderHistoryModel> _ListOrderHistory;
        public List<OrderHistoryModel> ListOrderHistory
        {
            get
            {
                return _ListOrderHistory;
            }
            set
            {
                if (_ListOrderHistory != value)
                {
                    _ListOrderHistory = value;
                    OnPropertyChanged("ListOrderHistory");
                }
            }
        }

        private string _PrevButton;
        public string PrevButton { get => _PrevButton; set { _PrevButton = value; OnPropertyChanged(); } }

        private string _NextButton;
        public string NextButton { get => _NextButton; set { _NextButton = value; OnPropertyChanged(); } }

        private int _pageIndex;
        public int pageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                if (_pageIndex != value)
                {
                    _pageIndex = value;
                    if (_pageIndex == 1)
                    {
                        PrevButton = "False";
                    }
                    else
                    {
                        PrevButton = "True";
                    }
                    OnPropertyChanged("pageIndex");
                }
            }
        }

        private readonly int pageSize;
        public int orderCount;
        #endregion

        #region Initial button command to binding button
        public ICommand AddOrderCommand { get; set; }
        public ICommand RemoveOrderCommand { get; set; }
        public ICommand SaveOrderCommand { get; set; }
        #endregion

        #region Constructor
        //Constructor - Xử lý tất cả event trong này !!
        public OrderViewModel()
        {
            _orderService = new OrderService();
            _productService = new ProductService();

            PrevButton = "False";
            NextButton = "True";
            pageIndex = 1;
            pageSize = 10;
            orderCount = _orderService.ListOrder().Count;

            SetupOrderDataGrid();
            GetListOrderHistory();

            AddOrderCommand = new RelayCommand<object>((p) =>
            {
                return true;
            }, (p) =>
            {
                AddOrder();
            });

            RemoveOrderCommand = new RelayCommand<object>((p) =>
            {
                return true;
            }, (p) =>
            {
                RemoveOrder();
            });

            SaveOrderCommand = new RelayCommand<object>((p) =>
            {
                return true;
            }, (p) =>
            {
                SubmitOrder();
                orderCount = _orderService.ListOrder().Count;
                GetListOrderHistory();
            });
        }
        #endregion

        #region Order Method
        private void SetupOrderDataGrid()
        {
            // Create 1 new row of order list.
            OrderList = new ObservableCollection<OrderModel>
            {
                new OrderModel()
            };

            // Get product list data from the database.
            ProductList = _productService.GetProducts();
        }

        //Add 1 order to order list
        public void AddOrder()
        {
            // Create 1 new row of order list.
            OrderList.Add(new OrderModel());
        }

        //Add 1 order to order list
        public void RemoveOrder()
        {
            // Create 1 new row of order list.
            OrderList.Remove(SelectedItem);
        }

        //Save order to database
        public void SubmitOrder()
        {
            // Get current user id from App.Current.Properties.
            //int userId = int.Parse(Application.Current.Properties["userId"].ToString());
            int userId = 1;

            // Create new dummy list to store data.
            List<Order> submitOrderList = new List<Order>();
            Order submitOrder;
            foreach (OrderModel order in _OrderList)
            {
                submitOrder = new Order();

                // Check if user input quantity or not.
                if (order.Quantity <= 0 || order.Quantity == null)
                {
                    MessageBox.Show("Please enter quantity you want to order.", "Error Message");
                    return;
                }

                submitOrder.Quantity = order.Quantity;
                submitOrder.ProductId = order.ProductId;
                submitOrder.UserId = userId;

                submitOrderList.Add(submitOrder);
            }

            // Function call the API to add Product and get response data.
            HttpResponseMessage response = _orderService.AddOrderList(submitOrderList);
            var responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                OrderList = new ObservableCollection<OrderModel>
                {
                    new OrderModel()
                };
            }
        }
        #endregion

        #region Order History method
        //Show history order
        private void GetListOrderHistory()
        {
            // Function call the API to get Order list.
            //List<Order> orderList = _orderService.ListOrder();
            List<Order> orderList = _orderService.GetAllPaging(pageSize, pageIndex);
            if (pageSize * pageIndex >= orderCount)
            {
                NextButton = "False";
            }
            else
            {
                NextButton = "True";
            }
            ListOrderHistory = new List<OrderHistoryModel>();
            foreach (Order order in orderList)
            {
                ListOrderHistory.Add(new OrderHistoryModel()
                {
                    Id = order.Id,
                    Name = order.Product.Name,
                    Detail = order.Product.Detail,
                    Unit = order.Product.Unit,
                    Price = order.Product.Price,
                    Quantity = order.Quantity,
                    TotalPrice = order.Product.Price * order.Quantity
                });
            }
        }
        #endregion

        #region Extend method to binding pagination
        //Method pagination
        public void PreviousPage()
        {
            pageIndex--;
            GetListOrderHistory();
        }
        public void NextPage()
        {
            pageIndex++;
            GetListOrderHistory();
        }
        #endregion

        #region Initial OrderModel to display for select product to submit
        //Order model to binding data
        public class OrderModel : BaseViewModel
        {
            private int _ProductId;
            public int ProductId
            {
                get
                {
                    return _ProductId;
                }
                set
                {
                    if (_ProductId != value)
                    {
                        _ProductId = value;
                        OnPropertyChanged("ProductId");
                        RefreshData(_ProductId);
                        CalculateTotalPrice(_Price, _Quantity);
                    }
                }
            }

            private string _Detail;
            public string Detail
            {
                get
                {
                    return _Detail;
                }
                set
                {
                    if (_Detail != value)
                    {
                        _Detail = value;
                        OnPropertyChanged("Detail");
                    }
                }
            }

            private string _Unit;
            public string Unit
            {
                get
                {
                    return _Unit;
                }
                set
                {
                    if (_Unit != value)
                    {
                        _Unit = value;
                        OnPropertyChanged("Unit");
                    }
                }
            }

            private double? _Price;
            public double? Price
            {
                get
                {
                    return _Price;
                }
                set
                {
                    if (_Price != value)
                    {
                        _Price = value;
                        OnPropertyChanged("Price");
                    }
                }
            }

            private int? _Quantity;
            public int? Quantity
            {
                get
                {
                    return _Quantity;
                }
                set
                {
                    if (_Quantity != value)
                    {
                        _Quantity = value;
                        OnPropertyChanged("Quantity");
                        CalculateTotalPrice(_Price, _Quantity);
                    }
                }
            }

            private double? _TotalPrice;
            public double? TotalPrice
            {
                get
                {
                    return _TotalPrice;
                }
                set
                {
                    if (_TotalPrice != value)
                    {
                        _TotalPrice = value;
                        OnPropertyChanged("TotalPrice");
                    }
                }
            }

            private void RefreshData(int _ProductId)
            {
                Product product = _ProductList.Find(p => p.Id == _ProductId);
                Detail = (product == null ? "" : product.Detail);
                Unit = (product == null ? "" : product.Unit);
                Price = (product == null ? 0 : product.Price);
            }

            private void CalculateTotalPrice(double? _Price, int? _Quantity)
            {
                TotalPrice = _Price * _Quantity;
            }

            //Validate input isnumber
            public void IsAllowedInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
            {
                Regex regex = new Regex("[^0-9]+");
                e.Handled = regex.IsMatch(e.Text);
            }
        }
        #endregion

        #region Initial OrderHistoryModel to display fields to List history order
        //OrderHistoryModel to binding history order
        public class OrderHistoryModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Detail { get; set; }
            public string Unit { get; set; }
            public double? Price { get; set; }
            public int? Quantity { get; set; }
            public double? TotalPrice { get; set; }
        }
        #endregion
    }
}