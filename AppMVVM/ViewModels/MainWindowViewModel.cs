﻿using AppMVVM.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AppMVVM.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Initial paramanter to binding to redirect
        public object _ViewModel;
        public object ViewModel
        {
            get
            {
                return _ViewModel;
            }
            set
            {
                if (_ViewModel != value)
                {
                    _ViewModel = value;
                    OnPropertyChanged("ViewModel");
                }
            }
        }
        #endregion

        #region Initial Button Command to binding Button event
        public bool Isloaded = false;
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand IndexCommand { get; set; }
        public ICommand ProductCommand { get; set; }
        public ICommand OrderCommand { get; set; }
        public ICommand AboutCommand { get; set; }
        #endregion

        #region Constructor
        public MainWindowViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                Isloaded = true;
                if (p == null)
                    return;
                p.Hide();
                LoginWindow loginWindow = new LoginWindow();
                loginWindow.ShowDialog();

                if (loginWindow.DataContext == null)
                    return;
                var loginVM = loginWindow.DataContext as LoginViewModel;

                if (loginVM.IsLogin)
                {
                    p.Show();
                    NavigateTo("Index");
                }
                else
                {
                    p.Close();
                }
            });

            IndexCommand = new RelayCommand<object>((p) =>
            {
                return true;
            }, (p) =>
            {
                NavigateTo("Index");
            });
            AboutCommand = new RelayCommand<object>((p) =>
            {
                return true;
            }, (p) =>
            {
                NavigateTo("About");
            });
            ProductCommand = new RelayCommand<object>((p) => 
            {
                return true;
            }, (p) => 
            {
                ProductWindow wd = new ProductWindow();
                wd.ShowDialog();
            });
            OrderCommand = new RelayCommand<object>((p) => 
            {
                return true;
            }, (p) => 
            {
                OrderWindow wd = new OrderWindow();
                wd.ShowDialog();
            });
        }
        #endregion

        #region Method to redirect - Binding to ViewModel
        public void NavigateTo(string destination)
        {
            if (destination.Equals("Index"))
            {
                _ViewModel = new IndexViewModel();
            }
            else if (destination.Equals("About"))
            {
                _ViewModel = new AboutViewModel();
            }
            OnPropertyChanged("ViewModel");
        }
        #endregion
    }
}
