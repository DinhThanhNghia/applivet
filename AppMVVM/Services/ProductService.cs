﻿using AppMVVM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AppMVVM.Services
{
    public class ProductService
    {
        private readonly CommonService commonServices = new CommonService();
        private readonly string apiControllerUrl = "/Product";

        public List<Product> GetProducts()
        {
            // Create API URL and call it to get response without json body (get method).
            HttpResponseMessage response = commonServices.HttpRequest(HttpMethod.Get, apiControllerUrl + "/", null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data and then return.
                return JsonConvert.DeserializeObject<List<Product>>(responseData);
            }

            MessageBox.Show(responseData, "Response Message");

            return null;
        }

        public List<Product> GetAllPaging(int pageSize, int pageIndex)
        {
            // Create API URL and call it to get response without json body (get method).
            HttpResponseMessage response = commonServices.HttpRequest(HttpMethod.Get,
                apiControllerUrl + "/GetAllPaging/_page=" + pageIndex + "&_limit=" + pageSize, null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data and then return.
                return JsonConvert.DeserializeObject<List<Product>>(responseData);
            }

            MessageBox.Show(responseData, "Response Message");

            return null;
        }

        public Product GetProductById(int productId)
        {
            // Create API URL and call it to get response without json body (get method).
            HttpResponseMessage response = commonServices.HttpRequest(HttpMethod.Get, apiControllerUrl + "/" + productId, null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data and then return.
                return JsonConvert.DeserializeObject<Product>(responseData);
            }

            MessageBox.Show(responseData, "Response Message");

            return null;
        }

        public HttpResponseMessage AddProduct(Product product)
        {
            // Create a JSON body to send with the request to the API.
            string jsonBody = "{" +
                   "'Name': '" + product.Name + "', " +
                   "'Detail': '" + product.Detail + "', " +
                   "'Price': '" + product.Price + "', " +
                   "'Status': '" + product.Status + "', " +
                   "'IsDeleted': '" + product.IsDeleted + "', " +
                   "'Unit': '" + product.Unit +
                   "'}";

            // Create API URL and call it to get response with json body (post method).
            HttpResponseMessage response = commonServices.HttpRequest(HttpMethod.Post, apiControllerUrl + "/", jsonBody);
            string responseData = response.Content.ReadAsStringAsync().Result;

            MessageBox.Show(responseData, "Response Message");

            return response;
        }

        public HttpResponseMessage EditProduct(Product product)
        {
            // Create a JSON body to send with the request to the API.
            string jsonBody = "{" +
                    "'Id': '" + product.Id + "', " +
                    "'Name': '" + product.Name + "', " +
                    "'Detail': '" + product.Detail + "', " +
                    "'Price': '" + product.Price + "', " +
                    "'Status': '" + product.Status + "', " +
                    "'IsDeleted': '" + product.IsDeleted + "', " +
                    "'Unit': '" + product.Unit +
                    "'}";

            // Create API URL and call it to get response with json body (post method).
            HttpResponseMessage response = commonServices.HttpRequest(HttpMethod.Put, apiControllerUrl + "/", jsonBody);
            string responseData = response.Content.ReadAsStringAsync().Result;

            MessageBox.Show(responseData, "Response Message");

            return response;
        }

        public HttpResponseMessage DeleteProduct(int productId)
        {
            // Create API URL and call it to get response without json body (delete method).
            HttpResponseMessage response = commonServices.HttpRequest(HttpMethod.Delete, apiControllerUrl + "/" + productId, null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            MessageBox.Show(responseData, "Response Message");

            return response;
        }
    }
}
