﻿using AppLivet.Helpers;
using Livet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AppLivet.Models
{
    public class Product : ValidationBase, INotifyPropertyChanged
    {
        public Product()
        {
            Orders = new HashSet<Order>();
        }

        public int Id { get; set; }

        private string _Name;
        [Required(ErrorMessage = "Product name is required")]
        [StringLength(255, ErrorMessage = "Max 255 characters")]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name != value)
                {
                    _Name = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("Name");
                }
            }
        }

        private string _Detail;
        [Required(ErrorMessage = "Detail is required")]
        [StringLength(255, ErrorMessage = "Max 255 characters")]
        public string Detail
        {
            get
            {
                return _Detail;
            }
            set
            {
                if (_Detail != value)
                {
                    _Detail = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("Detail");
                }
            }
        }

        public string _Unit;
        [Required(ErrorMessage = "Unit is required")]
        [StringLength(50, ErrorMessage = "Max 50 characters")]
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                if (_Unit != value)
                {
                    _Unit = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("Unit");
                }
            }
        }

        [Required(ErrorMessage = "Quantity is required")]
        [Range(0, 10000, ErrorMessage = "Price must be greater than 0 and less than 10000")]
        public int _Quantity;
        public int Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                if (_Quantity != value)
                {
                    _Quantity = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("_Quantity");
                }
            }
        }

        public string Status { get; set; }

        private string _UrlImage;
        public string UrlImage 
        {
            get
            {
                return _UrlImage;
            }
            set
            {
                if (_UrlImage != value)
                {
                    _UrlImage = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("UrlImage");
                }
            }
        }

        public double _Price;
        //[Required(ErrorMessage = "Price is required")]
        [Range(0.00, 999999999, ErrorMessage = "Price must be greater than 0.00 and less than 999999999")]
        public double Price
        {
            get
            {
                return _Price;
            }
            set
            {
                if (_Price != value)
                {
                    _Price = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("Price");
                }
            }
        }

        public DateTime CreatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<Order> Orders { set; get; }
    }
}