﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using AppLivet.Helpers;
using Livet;

namespace AppLivet.Models
{
    public class User : ValidationBase
    {
        public User()
        {
            Orders = new HashSet<Order>();
        }

        public int Id { get; set; }

        private string _UserName;
        [Required(ErrorMessage = "Username is required")]
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                if (_UserName != value)
                {
                    _UserName = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("UserName");
                }
            }
        }

        private string _Password;
        [Required(ErrorMessage = "Password is required")]
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                if (_Password != value)
                {
                    _Password = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("Password");
                }
            }
        }

        public virtual ICollection<Order> Orders { set; get; }
    }
}
