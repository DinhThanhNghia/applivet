﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Livet;

namespace AppLivet.Models
{
    public class Order : NotificationObject
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
    }
}
