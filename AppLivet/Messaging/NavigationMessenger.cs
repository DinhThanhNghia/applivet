﻿using Livet.Messaging;

namespace AppLivet.Messaging
{
    public static class NavigationMessenger
    {
        public static InteractionMessenger Messenger { get; } = new InteractionMessenger();
    }
}