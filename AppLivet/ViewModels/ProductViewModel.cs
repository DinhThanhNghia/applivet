﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using AppLivet.Services;
using System.Windows;
using System.Text.RegularExpressions;
using System.Net.Http;
using BespokeFusion;
using System.Windows.Media;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using System.IO;
using AppLivet.Helpers;
using AppLivet.Messaging;

namespace AppLivet.ViewModels
{
    public class ProductViewModel : ViewModel
    {
        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;

        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }

        public ProductViewModel()
        {
        }

        // Some useful code snippets for ViewModel are defined as l*(llcom, llcomn, lvcomm, lsprop, etc...).

        // This method would be called from View, when ContentRendered event was raised.
        public void Initialize()
        {
            Navigate("ListProduct");
        }
    }
}
