﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using AppLivet.Services;
using System.Collections.ObjectModel;
using AppLivet.Views;
using System.Net.Http;
using BespokeFusion;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.ComponentModel.DataAnnotations;
using AppLivet.Helpers;

namespace AppLivet.ViewModels
{
    public class ShoppingViewModel : ValidationBase
    {
        private readonly ProductService _productService;
        private readonly OrderService _orderService;

        private ObservableCollection<OrderModel> _OrderList;
        public ObservableCollection<OrderModel> OrderList
        {
            get => _OrderList;
            set
            {
                if (_OrderList != value)
                {
                    _OrderList = value;
                    RaisePropertyChanged("OrderList");
                }
            }
        }

        public static List<Product> _ProductList;
        public List<Product> ProductList
        {
            get => _ProductList;
            set
            {
                if (_ProductList != value)
                {
                    _ProductList = value;
                    RaisePropertyChanged("ProductList");
                }
            }
        }

        #region Information of product
        private int _Id;
        public int Id { get => _Id; set { _Id = value; RaisePropertyChanged(); } }

        private string _Name;
        public string Name
        {
            get => _Name;
            set
            {
                _Name = value;
                RaisePropertyChanged("Name");
            }
        }

        private string _Detail;
        public string Detail { get => _Detail; set { _Detail = value; RaisePropertyChanged("Detail"); } }

        private string _Status;
        public string Status { get => _Status; set { _Status = value; RaisePropertyChanged("Status"); } }

        private string _Unit;
        public string Unit { get => _Unit; set { _Unit = value; RaisePropertyChanged("Unit"); } }

        private int _Quantity;
        public int Quantity
        {
            get => _Quantity;
            set
            {
                if (_Quantity != value)
                {
                    _Quantity = value;
                    RaisePropertyChanged("Quantity");
                }
            }
        }

        private string _UrlImage;
        public string UrlImage
        {
            get => _UrlImage;
            set
            {
                if (_UrlImage != value)
                {
                    _UrlImage = value;
                    RaisePropertyChanged("UrlImage");
                }
            }
        }

        private double? _Price;
        public double? Price
        {
            get => _Price;
            set
            {
                if (_Price != value)
                {
                    _Price = value;
                    RaisePropertyChanged("Price");
                }
            }
        }

        private string _Email;
        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$",
            ErrorMessage = "Isvalid format")]
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                if (_Email != value)
                {
                    _Email = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("Email");
                }
            }
        }
        private System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

        private string _NotFound;
        public string NotFound
        {
            get
            {
                return _NotFound;
            }
            set
            {
                if (_NotFound != value)
                {
                    _NotFound = value;
                    RaisePropertyChanged("NotFound");
                }
            }
        }

        private string _Keyword;
        public string Keyword
        {
            get
            {
                return _Keyword;
            }
            set
            {
                if (_Keyword != value)
                {
                    _Keyword = value;
                    ValidateProperty(value);
                    base.RaisePropertyChanged("Keyword");
                }
            }
        }

        public void Search()
        {
            var products = _productService.GetProducts().Where(x => x.Name.ToLower().Contains(Keyword.ToLower()) || x.Detail.ToLower().Contains(Keyword.ToLower()));
            if (products.Count() > 0)
            {
                ProductList = products.ToList();
            }
            else
            {
                ProductList = null;
                NotFound = "No product same as keyword";
            }
        }

        #endregion

        #region Button Command and selected item
        private Product _SelectedItem;
        public Product SelectedItem
        {
            get => _SelectedItem;
            set
            {
                _SelectedItem = value;
                RaisePropertyChanged();
                if (SelectedItem != null)
                {
                    Id = SelectedItem.Id;
                    Name = SelectedItem.Name;
                    Detail = SelectedItem.Detail;
                    Unit = SelectedItem.Unit;
                    Price = SelectedItem.Price;
                    Quantity = SelectedItem.Quantity;
                    UrlImage = SelectedItem.UrlImage;
                }
            }
        }

        private OrderModel _SelectedCartItem;
        public OrderModel SelectedCartItem
        {
            get => _SelectedCartItem;
            set
            {
                _SelectedCartItem = value;
                RaisePropertyChanged();
            }
        }

        private ViewModelCommand _EmptyCommand;
        public ViewModelCommand EmptyCommand
        {
            get
            {
                if (_EmptyCommand == null)
                {
                    _EmptyCommand = new ViewModelCommand(EmptyCart);
                }
                return _EmptyCommand;
            }
        }

        private ViewModelCommand _PurchaseCommand;
        public ViewModelCommand PurchaseCommand
        {
            get
            {
                if (_PurchaseCommand == null)
                {
                    _PurchaseCommand = new ViewModelCommand(AddToCart);
                }
                return _PurchaseCommand;
            }
        }

        private ViewModelCommand _RemoveOrderCommand;
        public ViewModelCommand RemoveOrderCommand
        {
            get
            {
                if (_RemoveOrderCommand == null)
                {
                    _RemoveOrderCommand = new ViewModelCommand(RemoveOrder);
                }
                return _RemoveOrderCommand;
            }
        }

        private ViewModelCommand _SaveOrderCommand;
        public ViewModelCommand SaveOrderCommand
        {
            get
            {
                if (_SaveOrderCommand == null)
                {
                    _SaveOrderCommand = new ViewModelCommand(SubmitOrder);
                }
                return _SaveOrderCommand;
            }
        }
        #endregion

        #region Private Function
        private int _CountOrder;
        public int CountOrder
        {
            get => _CountOrder;
            set
            {
                _CountOrder = value;
                RaisePropertyChanged("CountOrder");
            }
        }

        private void EmptyCart()
        {
            OrderList = new ObservableCollection<OrderModel>();
            CountOrder = 0;
        }

        private void AddToCart()
        {
            if (SelectedItem != null)
            {
                if (SelectedItem.Quantity > 0)
                {
                    var isExist = OrderList.Where(x => x.ProductId == SelectedItem.Id).Count();

                    if (isExist > 0)
                    {
                        foreach (var item in OrderList)
                        {
                            try
                            {
                                if (SelectedItem.Id == item.ProductId)
                                {
                                    item.Quantity += 1;
                                    MaterialMessageBox.Show("Add to cart successfully!");
                                }
                            }
                            catch (Exception)
                            {
                                return;
                            }
                        }
                    }
                    else
                    {
                        OrderList.Add(new OrderModel
                        {
                            ProductId = SelectedItem.Id,
                            Name = SelectedItem.Name,
                            Detail = SelectedItem.Detail,
                            UrlImage = SelectedItem.UrlImage,
                            Unit = SelectedItem.Unit,
                            Price = SelectedItem.Price,
                            Quantity = 1
                        });
                        CountOrder = OrderList.Count();
                        MaterialMessageBox.Show("Add to cart successfully!");
                    }
                }
                else
                {
                    MaterialMessageBox.ShowError("This product is out of stock. Please select different product!");
                }
            }
            else
            {
                MaterialMessageBox.ShowError("Please choose product and purchase!");
            }
        }

        private void RemoveOrder()
        {
            if (SelectedCartItem != null)
            {
                OrderList.Remove(SelectedCartItem);
                CountOrder = OrderList.Count();
            }
            else
            {
                MaterialMessageBox.ShowError("Cart is empty!");
            }
        }

        private void SendingEmail(string txtContent)
        {
            if (string.IsNullOrEmpty(Email) || !regex.IsMatch(Email))
            {
                return;
            }
            else
            {
                var smtpServerName = ConfigurationManager.AppSettings["SmtpServer"];
                var port = ConfigurationManager.AppSettings["Port"];
                var senderEmailId = ConfigurationManager.AppSettings["SenderEmailId"];
                var senderPassword = ConfigurationManager.AppSettings["SenderPassword"];

                var smptClient = new SmtpClient(smtpServerName, Convert.ToInt32(port))
                {
                    Credentials = new NetworkCredential(senderEmailId, senderPassword),
                    EnableSsl = true
                };
                string txtSubject = "Email to follow order";
                smptClient.Send(senderEmailId, Email, txtSubject, txtContent);
                MaterialMessageBox.Show("Message Sent Successfully");
            }
        }
       
        private void SubmitOrder()
        {
            string mailContent = "";
            double? totalAmount = 0;
            if (CountOrder > 0)
            {
                // Get current user id from App.Current.Properties.
                int userId = 1;
                // Create new dummy list to store data.
                List<Order> submitOrderList = new List<Order>();
                Order submitOrder;
                foreach (OrderModel order in OrderList)
                {
                    submitOrder = new Order();

                    if (string.IsNullOrEmpty(order.Quantity.ToString()))
                    {
                        MaterialMessageBox.ShowError("Please enter quantity you want to order.");
                        return;
                    }
                    if (order.Quantity <= 0)
                    {
                        MaterialMessageBox.ShowError("Please enter quantity you want to order.");
                        return;
                    }
                    var products = _productService.GetProducts().Where(x => x.Id == order.ProductId);
                    foreach (var item in products)
                    {
                        if (order.Quantity > item.Quantity)
                        {
                            MaterialMessageBox.ShowError("Quantity of " + item.Name + " is greater than quantity in system.");
                            return;
                        }
                        if (order.Quantity <= item.Quantity)
                        {
                            item.Quantity -= order.Quantity;
                            _productService.EditProduct(item);
                        }
                    }

                    submitOrder.Quantity = order.Quantity;
                    submitOrder.ProductId = order.ProductId;
                    submitOrder.UserId = userId;

                    submitOrderList.Add(submitOrder);
                    totalAmount += order.Price * order.Quantity;

                }

                // Function call the API to add Product and get response data.
                HttpResponseMessage response = _orderService.AddOrderList(submitOrderList);
                var responseData = response.Content.ReadAsStringAsync().Result;
                mailContent = "Thank you for your order! \n" +
                              "Total amount: $" + totalAmount + "\n" +
                              "Order date: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm") + "\n" +
                              "App Livet. Demo sending an Email. Thank you!";
                if (response.IsSuccessStatusCode)
                {
                    OrderList = new ObservableCollection<OrderModel>();
                    CountOrder = 0;
                    SendingEmail(mailContent);
                }
            }
            else
            {
                MaterialMessageBox.ShowError("No product to submit");
            }
        }
        #endregion

        #region Constructor
        public ShoppingViewModel()
        {
            _productService = new ProductService();
            _orderService = new OrderService();
            CountOrder = 0;
            OrderList = new ObservableCollection<OrderModel>();
            SetupOrderDataGrid();
        }
        #endregion

        private void SetupOrderDataGrid()
        {
            var products = _productService.GetProducts();
            foreach (var item in products)
            {
                item.Status = item.Quantity > 0 ? "InStock" : "Out of Stock";
            }
            ProductList = products;
        }

        #region Initial OrderModel to display for select product to submit
        //Order model to binding data
        public class OrderModel : ViewModel
        {
            private int _ProductId;
            public int ProductId
            {
                get
                {
                    return _ProductId;
                }
                set
                {
                    if (_ProductId != value)
                    {
                        _ProductId = value;
                        RaisePropertyChanged("ProductId");
                        RefreshData(_ProductId);
                        CalculateTotalPrice(_Price, _Quantity);
                    }
                }
            }

            private string _Name;
            public string Name
            {
                get
                {
                    return _Name;
                }
                set
                {
                    if (_Name != value)
                    {
                        _Name = value;
                        RaisePropertyChanged("Name");
                    }
                }
            }

            private string _Detail;
            public string Detail
            {
                get
                {
                    return _Detail;
                }
                set
                {
                    if (_Detail != value)
                    {
                        _Detail = value;
                        RaisePropertyChanged("Detail");
                    }
                }
            }

            private string _UrlImage;
            public string UrlImage
            {
                get
                {
                    return _UrlImage;
                }
                set
                {
                    if (_UrlImage != value)
                    {
                        _UrlImage = value;
                        RaisePropertyChanged("UrlImage");
                    }
                }
            }

            private string _Unit;
            public string Unit
            {
                get
                {
                    return _Unit;
                }
                set
                {
                    if (_Unit != value)
                    {
                        _Unit = value;
                        RaisePropertyChanged("Unit");
                    }
                }
            }

            private double? _Price;
            public double? Price
            {
                get
                {
                    return _Price;
                }
                set
                {
                    if (_Price != value)
                    {
                        _Price = value;
                        RaisePropertyChanged("Price");
                    }
                }
            }

            private int _Quantity;
            public int Quantity
            {
                get
                {
                    return _Quantity;
                }
                set
                {
                    if (_Quantity != value)
                    {
                        _Quantity = value;
                        RaisePropertyChanged("Quantity");
                        CalculateTotalPrice(_Price, _Quantity);
                    }
                }
            }

            private double? _TotalPrice;
            public double? TotalPrice
            {
                get
                {
                    return _TotalPrice;
                }
                set
                {
                    if (_TotalPrice != value)
                    {
                        _TotalPrice = value;
                        RaisePropertyChanged("TotalPrice");
                    }
                }
            }

            private void RefreshData(int _ProductId)
            {
                Product product = _ProductList.Find(p => p.Id == _ProductId);
                Detail = (product == null ? "" : product.Detail);
                Unit = (product == null ? "" : product.Unit);
                Price = (product == null ? 0 : product.Price);
            }

            private void CalculateTotalPrice(double? _Price, int? _Quantity)
            {
                TotalPrice = _Price * _Quantity;
            }

            //Validate input isnumber
            public void IsAllowedInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
            {
                Regex regex = new Regex("[^0-9]+");
                e.Handled = regex.IsMatch(e.Text);
            }
        }
        #endregion
        // Some useful code snippets for ViewModel are defined as l*(llcom, llcomn, lvcomm, lsprop, etc...).

        // This method would be called from View, when ContentRendered event was raised.
        public void Initialize()
        {
        }
    }
}
