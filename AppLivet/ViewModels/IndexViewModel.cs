﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using System.Windows;
using AppLivet.Services;

namespace AppLivet.ViewModels
{
    public class IndexViewModel : ViewModel
    {
        private readonly OrderService _orderService;
        private readonly ProductService _productService;
        private string _UserName;
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                if (_UserName != value)
                {
                    _UserName = value;
                    RaisePropertyChanged("UserName");
                }
            }
        }

        private int _NumberProduct;
        public int NumberProduct
        {
            get
            {
                return _NumberProduct;
            }
            set
            {
                if (_NumberProduct != value)
                {
                    _NumberProduct = value;
                    RaisePropertyChanged("NumberProduct");
                }
            }
        }

        private int _NumberOrder;
        public int NumberOrder
        {
            get
            {
                return _NumberOrder;
            }
            set
            {
                if (_NumberOrder != value)
                {
                    _NumberOrder = value;
                    RaisePropertyChanged("NumberOrder");
                }
            }
        }

        private int _AllQuantity;
        public int AllQuantity
        {
            get
            {
                return _AllQuantity;
            }
            set
            {
                if (_AllQuantity != value)
                {
                    _AllQuantity = value;
                    RaisePropertyChanged("AllQuantity");
                }
            }
        }

        private double _AllPrice;
        public double AllPrice
        {
            get
            {
                return _AllPrice;
            }
            set
            {
                if (_AllPrice != value)
                {
                    _AllPrice = value;
                    RaisePropertyChanged("AllPrice");
                }
            }
        }
        public IndexViewModel()
        {
            _productService = new ProductService();
            _orderService = new OrderService();

            UserName = "admin";

            var products = _productService.GetProducts();
            var orders = _orderService.ListOrder();

            NumberProduct = products.Count();
            NumberOrder = orders.Count();
            AllQuantity = 0;
            AllPrice = 0;
            foreach (Order order in orders)
            {
                AllQuantity += (order.Quantity == 0 ? 0 : order.Quantity);
                AllPrice += order.Product.Price == 0 ? 0 : order.Product.Price * order.Quantity;
            }
        }

        // Some useful code snippets for ViewModel are defined as l*(llcom, llcomn, lvcomm, lsprop, etc...).

        // This method would be called from View, when ContentRendered event was raised.
        //public void Initialize()
        //{
        //}
    }
}
