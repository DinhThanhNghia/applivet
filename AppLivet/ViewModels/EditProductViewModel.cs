﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using AppLivet.Services;
using BespokeFusion;
using AppLivet.Messaging;
using Microsoft.Win32;
using System.IO;
using System.Text.RegularExpressions;

namespace AppLivet.ViewModels
{
    public class EditProductViewModel : ViewModel
    {
        private readonly ProductService _productService;

        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;
        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }

        private Product _Product;
        public Product Product
        {
            get
            {
                return _Product;
            }
            set
            {
                if (_Product != value)
                {
                    _Product = value;
                    RaisePropertyChanged("Product");
                }
            }
        }

        private ViewModelCommand _EditCommand;
        public ViewModelCommand EditCommand
        {
            get
            {
                if (_EditCommand == null)
                {
                    _EditCommand = new ViewModelCommand(EditProduct);
                }
                return _EditCommand;
            }
        }

        private ViewModelCommand _ResetCommand;
        public ViewModelCommand ResetCommand
        {
            get
            {
                if (_ResetCommand == null)
                {
                    _ResetCommand = new ViewModelCommand(ResetProduct);
                }
                return _ResetCommand;
            }
        }

        private ViewModelCommand _UploadCommand;
        public ViewModelCommand UploadCommand
        {
            get
            {
                if (_UploadCommand == null)
                {
                    _UploadCommand = new ViewModelCommand(UploadImage);
                }
                return _UploadCommand;
            }
        }

        public EditProductViewModel()
        {
            _productService = new ProductService();
            GetProduct();
        }

        //Private function
        private void GetProduct()
        {
            int productId = int.Parse(App.Current.Properties["Id"].ToString());

            // Get product by productId.
            Product = _productService.GetProductById(productId);
            if (!string.IsNullOrEmpty(Product.UrlImage))
            {
                ImportPath = Product.UrlImage;
            }
        }

        private void EditProduct()
        {
            //Validate data
            bool returnFlag = false;
            if (string.IsNullOrWhiteSpace(Product.Name) || Product.Name.Length >= 255)
            {
                Product.Name = "";
                returnFlag = true;
            }
            if (string.IsNullOrWhiteSpace(Product.Detail) || Product.Detail.Length >= 255)
            {
                Product.Detail = "";
                returnFlag = true;
            }
            if (string.IsNullOrWhiteSpace(Product.Unit) || Product.Unit.Length > 50)
            {
                Product.Unit = "";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Product.Quantity.ToString()))
            {
                Product.Quantity = 0;
                returnFlag = true;
            }
            if (Product.Quantity < 0 || Product.Quantity > 10000)
            {
                MaterialMessageBox.ShowError("Validation Message:" + Environment.NewLine + "- Quantity must be greater than -1 and less than 10000.");
            }
            if (string.IsNullOrEmpty(Product.Price.ToString()))
            {
                Product.Price = 0;
                returnFlag = true;
            }
            if (Product.Price <= 0 || Product.Price > 999999999)
            {
                MaterialMessageBox.ShowError("Validation Message:" + Environment.NewLine + "- Price must be greater than 0 and less than 999999999.");
            }
            if (returnFlag)
            {
                return;
            }
            else
            {
                var response = _productService.EditProduct(Product);
                var responseData = response.Content.ReadAsStringAsync().Result;
                MaterialMessageBox.Show(responseData);
                Navigate("ListProduct");
            }
        }

        private void ResetProduct()
        {
            Product = new Product();
            ImportPath = "";
        }

        private string _ImportPath;
        public string ImportPath
        {
            get
            {
                return _ImportPath;
            }
            set
            {
                if (_ImportPath != value)
                {
                    _ImportPath = value;
                    RaisePropertyChanged("ImportPath");
                }
            }
        }

        private string ImportName;
        //private string DestinationPath;
        private void UploadImage()
        {
            //upload image
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select Image:";

            if (op.ShowDialog() == true)
            {
                ImportPath = op.FileName;
                string[] _padGesplitst = ImportPath.Split('\\');
                ImportName = _padGesplitst[(_padGesplitst.Length - 18)];
                string _destinationFullPath = System.AppDomain.CurrentDomain.BaseDirectory;
                Product.UrlImage = _destinationFullPath.Remove(_destinationFullPath.Length - 1) + "\\Assets\\Images\\" + ImportName;
                Product.UrlImage = Product.UrlImage.Replace("\\", "/");
            }
            try
            {
                File.Copy(ImportPath, this.Product.UrlImage, true);
                MaterialMessageBox.Show("Your images is added with name: " + ImportName);
            }
            catch
            {
                MaterialMessageBox.Show("File is exist in system");
            }
        }

        public void OnlyNumber(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        // Some useful code snippets for ViewModel are defined as l*(llcom, llcomn, lvcomm, lsprop, etc...).

        // This method would be called from View, when ContentRendered event was raised.
        public void Initialize()
        {
        }
    }
}
