﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using AppLivet.Services;
using System.Net.Http;
using System.Windows;
using BespokeFusion;
using AppLivet.Messaging;

namespace AppLivet.ViewModels
{
    public class ListProductViewModel : ViewModel
    {
        private readonly ProductService _productService;
        private readonly OrderService _orderService;

        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;
        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }

        private List<Product> _List;
        public List<Product> List { get => _List; set { _List = value; RaisePropertyChanged(); } }

        private string _PrevButton;
        public string PrevButton
        {
            get
            {
                return _PrevButton;
            }
            set
            {
                if (_PrevButton != value)
                {
                    _PrevButton = value;
                    RaisePropertyChanged("PrevButton");
                }
            }
        }

        private string _NextButton;
        public string NextButton
        {
            get
            {
                return _NextButton;
            }
            set
            {
                if (_NextButton != value)
                {
                    _NextButton = value;
                    RaisePropertyChanged("NextButton");
                }
            }
        }

        private int _PageIndex;
        public int PageIndex
        {
            get
            {
                return _PageIndex;
            }
            set
            {
                if (_PageIndex != value)
                {
                    _PageIndex = value;
                    if (_PageIndex == 1)
                    {
                        PrevButton = "False";
                    }
                    else
                    {
                        PrevButton = "True";
                    }
                    RaisePropertyChanged("PageIndex");
                }
            }
        }

        private readonly int pageSize;
        private int _CurrentPage;

        public int CurrentPage
        {
            get
            {
                return _CurrentPage;
            }
            set
            {
                if (_CurrentPage != value)
                {
                    _CurrentPage = value;
                    RaisePropertyChanged("CurrentPage");
                }
            }
        }
        public int productCount;

        //Command
        private ListenerCommand<int> _ShowEditCommand;

        public ListenerCommand<int> ShowEditCommand
        {
            get
            {
                if (_ShowEditCommand == null)
                {
                    _ShowEditCommand = new ListenerCommand<int>(ShowEditProduct);
                }
                return _ShowEditCommand;
            }
        }

        private ListenerCommand<int> _DeleteCommand;

        public ListenerCommand<int> DeleteCommand
        {
            get
            {
                if (_DeleteCommand == null)
                {
                    _DeleteCommand = new ListenerCommand<int>(DeleteProduct);
                }
                return _DeleteCommand;
            }
        }
        public ListProductViewModel()
        {
            _productService = new ProductService();
            _orderService = new OrderService();


            PrevButton = "False";
            NextButton = "True";
            PageIndex = 1;
            pageSize = 5;

            GetProducts();
        }

        //Private funtion GET - DELETE
        private void GetProducts()
        {
            //Count product to get current page
            productCount = _productService.GetProducts().Count;
            // Count current page
            CurrentPage = (productCount % pageSize) > 0 ? (productCount / pageSize) + 1 : (productCount / pageSize);

            // Get product list.
            var products = _productService.GetAllPaging(pageSize, PageIndex);
            foreach (var item in products)
            {
                item.Status = item.Quantity > 0 ? "InStock" : "UnStock";
            }
            if (pageSize * PageIndex >= productCount)
            {
                NextButton = "False";
            }
            else
            {
                NextButton = "True";
            }
            List = products;
        }

        private void DeleteProduct(int id)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Delete this Product?", "Delete Confirmation", MessageBoxButton.YesNo);

            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Completing the URL and call the API to get response data.
                List<Order> orders = _orderService.ListOrder();
                var result = orders.Where(x => x.Product.Id == id).Count();
                if (result > 0)
                {
                    MaterialMessageBox.ShowError("This record was used in Order table.");
                }
                else
                {
                    HttpResponseMessage response = _productService.DeleteProduct(id);
                    if (response.IsSuccessStatusCode)
                    {
                        GetProducts();
                    }
                }
            }
        }

        private void ShowEditProduct(int id)
        {
            App.Current.Properties["Id"] = id;
            Navigate("EditProduct");
        }

        //Method pagination
        public void PreviousPage()
        {
            PageIndex--;
            GetProducts();
        }
        public void NextPage()
        {
            PageIndex++;
            GetProducts();
        }

        // Some useful code snippets for ViewModel are defined as l*(llcom, llcomn, lvcomm, lsprop, etc...).

        // This method would be called from View, when ContentRendered event was raised.
        public void Initialize()
        {
        }
    }
}
