﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using AppLivet.Services;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Net.Http;
using BespokeFusion;
using System.Windows;
using System.Windows.Controls;
using EO.Internal;
using System.Windows.Media;

namespace AppLivet.ViewModels
{
    public class OrderViewModel : ViewModel
    {
        private readonly OrderService _orderService;
        private readonly ProductService _productService;

        #region Initial paramanter to binding
        //Initial OrderList to add new order to database
        private OrderHistoryModel _SelectedItemHistory;
        public OrderHistoryModel SelectedItemHistory { get => _SelectedItemHistory; set { _SelectedItemHistory = value; RaisePropertyChanged("SelectedItemHistory"); } }

        //Initial HistoryOrderList to show history Order
        public List<OrderHistoryModel> _ListOrderHistory;
        public List<OrderHistoryModel> ListOrderHistory
        {
            get
            {
                return _ListOrderHistory;
            }
            set
            {
                if (_ListOrderHistory != value)
                {
                    _ListOrderHistory = value;
                    RaisePropertyChanged("ListOrderHistory");
                }
            }
        }

        private string _PrevButton;
        public string PrevButton
        {
            get => _PrevButton;
            set
            {
                if (_PrevButton != value)
                {
                    _PrevButton = value;
                    RaisePropertyChanged("PrevButton");
                }
            }
        }

        private string _NextButton;
        public string NextButton
        {
            get => _NextButton;
            set
            {
                if (_NextButton != value)
                {
                    _NextButton = value;
                    RaisePropertyChanged("NextButton");
                }
            }
        }

        private int _PageIndex;
        public int PageIndex
        {
            get
            {
                return _PageIndex;
            }
            set
            {
                if (_PageIndex != value)
                {
                    _PageIndex = value;
                    if (_PageIndex == 1)
                    {
                        PrevButton = "False";
                    }
                    else
                    {
                        PrevButton = "True";
                    }
                    RaisePropertyChanged("PageIndex");
                }
            }
        }

        private int _CurrentPage;
        public int CurrentPage
        {
            get
            {
                return _CurrentPage;
            }
            set
            {
                if (_CurrentPage != value)
                {
                    _CurrentPage = value;
                    RaisePropertyChanged("CurrentPage");
                }
            }
        }

        private readonly int pageSize;
        public int orderCount;
        #endregion

        #region Initial button command to binding button
        private ListenerCommand<int> _DeleteCommand;
        public ListenerCommand<int> DeleteCommand
        {
            get
            {
                if (_DeleteCommand == null)
                {
                    _DeleteCommand = new ListenerCommand<int>(DeleteOrder);
                }
                return _DeleteCommand;
            }
        }
        #endregion

        #region Constructor
        public OrderViewModel()
        {
            _productService = new ProductService();
            _orderService = new OrderService();

            PrevButton = "False";
            NextButton = "True";
            PageIndex = 1;
            pageSize = 5;

            GetListOrderHistory();
        }
        #endregion

        #region Order Method
        private void DeleteOrder(int id)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Delete this Order?", "Delete Confirmation", MessageBoxButton.YesNo);

            if (messageBoxResult == MessageBoxResult.Yes)
            {
                var order = _orderService.ListOrder().Where(x => x.Id == id).FirstOrDefault();
                var product = _productService.GetProducts().Where(x => x.Id == order.ProductId).FirstOrDefault();
                product.Quantity += order.Quantity;
                _productService.EditProduct(product);

                HttpResponseMessage response = _orderService.DeleteOrder(id);

                if (response.IsSuccessStatusCode)
                {
                    GetListOrderHistory();
                }
            }
        }
        #endregion

        #region Order History method
        //Show history order
        private void GetListOrderHistory()
        {
            //Count order to get current page
            orderCount = _orderService.ListOrder().Count;
            //Get current page
            CurrentPage = (orderCount % pageSize) > 0 ? (orderCount / pageSize) + 1 : (orderCount / pageSize);

            List<Order> orderList = _orderService.GetAllPaging(pageSize, PageIndex);
            if (pageSize * PageIndex >= orderCount)
            {
                NextButton = "False";
            }
            else
            {
                NextButton = "True";
            }
            ListOrderHistory = new List<OrderHistoryModel>();
            foreach (Order order in orderList)
            {
                ListOrderHistory.Add(new OrderHistoryModel()
                {
                    Id = order.Id,
                    Name = order.Product.Name,
                    Detail = order.Product.Detail,
                    UrlImage = order.Product.UrlImage,
                    Unit = order.Product.Unit,
                    Price = order.Product.Price,
                    Quantity = order.Quantity,
                    CreatedDate = order.CreatedDate,
                    TotalPrice = order.Product.Price * order.Quantity
                });
            }
        }
        #endregion

        #region Extend method to binding pagination
        //Method pagination
        public void PreviousPage()
        {
            PageIndex--;
            GetListOrderHistory();
        }
        public void NextPage()
        {
            PageIndex++;
            GetListOrderHistory();
        }
        #endregion

        #region Initial OrderHistoryModel to display fields to List history order
        //OrderHistoryModel to binding history order
        public class OrderHistoryModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string UrlImage { get; set; }
            public string Detail { get; set; }
            public string Unit { get; set; }
            public double? Price { get; set; }
            public int? Quantity { get; set; }
            public double? TotalPrice { get; set; }
            public DateTime CreatedDate { get; set; }
        }
        #endregion
        // This method would be called from View, when ContentRendered event was raised.
        public void Initialize()
        {
        }
    }
}
