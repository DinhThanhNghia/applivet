﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using Newtonsoft.Json;
using System.Windows;
using System.Net.Http;
using AppLivet.Services;
using System.Windows.Controls;
using BespokeFusion;
using System.ComponentModel.DataAnnotations;

namespace AppLivet.ViewModels
{
    public class LoginViewModel : ViewModel
    {
        private readonly UserService _userService;

        public bool IsLogin { get; set; }
        private string _UserName;
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                if (_UserName != value)
                {
                    _UserName = value;
                    RaisePropertyChanged("UserName");
                }
            }
        }
        private string _Password;
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                if (_Password != value)
                {
                    _Password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }

        public ListenerCommand<Window> LoginCommand { get; set; }
        public ListenerCommand<PasswordBox> PasswordChangedCommand { get; set; }

        public LoginViewModel()
        {
            IsLogin = false;
            _userService = new UserService();

            LoginCommand = new ListenerCommand<Window>((p) => 
            { 
                // Close Login Window
                Login(p);
            });

            PasswordChangedCommand = new ListenerCommand<PasswordBox>((p) =>
            {
                Password = p.Password;
            });
        }

        private void Login(Window p)
        {
            // Completing the URL and call the API to get response data.
            HttpResponseMessage response = _userService.Login(UserName, Password);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (p == null)
                return;
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
            {
                IsLogin = false;
                MaterialMessageBox.ShowError("User name and Password has been required");
            }
            else if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data.
                var data = JsonConvert.DeserializeObject<User>(responseData);

                // Set token data and username to App.Current.Properties. These work like global variables.
                Application.Current.Properties["userId"] = data.Id;
                Application.Current.Properties["username"] = UserName;

                IsLogin = true;
                p.Close();
            }
            else
            {
                IsLogin = false;
                MaterialMessageBox.ShowError("User name or password was failed. Please check again!");
            }
        }
        // Some useful code snippets for ViewModel are defined as l*(llcom, llcomn, lvcomm, lsprop, etc...).

        // This method would be called from View, when ContentRendered event was raised.
        public void Initialize()
        {
        }
    }
}
