﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AppLivet.Models;
using AppLivet.Views;
using System.Windows;

namespace AppLivet.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        #region Initial paramanter to binding to redirect
        public object _ViewModel;
        public object ViewModel
        {
            get
            {
                return _ViewModel;
            }
            set
            {
                if (_ViewModel != value)
                {
                    _ViewModel = value;
                    RaisePropertyChanged("ViewModel");
                }
            }
        }
        #endregion

        #region Initial Button Command to binding Button event
        public bool Isloaded = false;
        public ListenerCommand<Window> _LoadedWindowCommand { get; set; }
        public ListenerCommand<Window> LoadedWindowCommand
        {
            get
            {
                if (_LoadedWindowCommand == null)
                {
                    _LoadedWindowCommand = new ListenerCommand<Window>((p)=> 
                    {
                        Isloaded = true;
                        if (p == null)
                            return;
                        p.Hide();
                        LoginWindow loginWindow = new LoginWindow();
                        loginWindow.ShowDialog();

                        if (loginWindow.DataContext == null)
                            return;
                        var loginVM = loginWindow.DataContext as LoginViewModel;

                        if (loginVM.IsLogin)
                        {
                            p.Show();
                            NavigateTo("Index");
                        }
                        else
                        {
                            p.Close();
                        }
                    });
                }
                return _LoadedWindowCommand;
            }
        }

        public ViewModelCommand _IndexCommand { get; set; }
        public ViewModelCommand IndexCommand
        {
            get
            {
                if (_IndexCommand == null)
                {
                    _IndexCommand = new ViewModelCommand(ShowIndexView);
                }
                return _IndexCommand;
            }
        }
        public void ShowIndexView()
        {
            NavigateTo("Index");
        }

        public ViewModelCommand _ProductCommand { get; set; }
        public ViewModelCommand ProductCommand
        {
            get
            {
                if (_ProductCommand == null)
                {
                    _ProductCommand = new ViewModelCommand(ShowProductWindow);
                }
                return _ProductCommand;
            }
        }
        public void ShowProductWindow()
        {
            ProductWindow wd = new ProductWindow();
            wd.ShowDialog();
        }

        public ViewModelCommand _OrderCommand { get; set; }
        public ViewModelCommand OrderCommand
        {
            get
            {
                if (_OrderCommand == null)
                {
                    _OrderCommand = new ViewModelCommand(ShowOrderWindow);
                }
                return _OrderCommand;
            }
        }
        public void ShowOrderWindow()
        {
            OrderWindow wd = new OrderWindow();
            wd.ShowDialog();
        }

        public ViewModelCommand _ShoppingCommand { get; set; }
        public ViewModelCommand ShoppingCommand
        {
            get
            {
                if (_ShoppingCommand == null)
                {
                    _ShoppingCommand = new ViewModelCommand(ShowShoppingView);
                }
                return _ShoppingCommand;
            }
        }
        public void ShowShoppingView()
        {
            ShoppingWindow wd = new ShoppingWindow();
            wd.ShowDialog();
        }
        #endregion

        #region Constructor
        public MainWindowViewModel()
        {
            NavigateTo("Index");
        }
        #endregion Constructor

        #region Method to redirect - Binding to ViewModel
        public void NavigateTo(string destination)
        {
            if (destination.Equals("Index"))
            {
                _ViewModel = new IndexViewModel();
            }
            RaisePropertyChanged("ViewModel");
        }
        #endregion

        // Some useful code snippets for ViewModel are defined as l*(llcom, llcomn, lvcomm, lsprop, etc...).
        public void Initialize()
        {
        }
    }
}
