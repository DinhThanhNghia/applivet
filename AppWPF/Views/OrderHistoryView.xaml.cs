﻿using AppWPF.Models;
using AppWPF.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppWPF.Views
{
    public partial class OrderHistoryView : UserControl
    {
        private readonly OrderService orderService = new OrderService();

        public OrderHistoryView()
        {
            InitializeComponent();
            ListOrder();
        }

        private async void ListOrder()
        {
            // Function call the API to get Order list.
            List<Order> orderList = await orderService.ListOrder();
            List<OrderHistory> orderHistoryList = new List<OrderHistory>();
            foreach (Order order in orderList)
            {
                orderHistoryList.Add(new OrderHistory()
                {
                    Id = order.Id,
                    Name = order.Product.Name,
                    Detail = order.Product.Detail,
                    Unit = order.Product.Unit,
                    Price = order.Product.Price,
                    Quantity = order.Quantity,
                    TotalPrice = order.Product.Price * order.Quantity
                });
            }
            dataGridOrderList.ItemsSource = orderHistoryList;
        }
    }

    internal class OrderHistory
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
        public string Unit { get; set; }
        public double? Price { get; set; }
        public int? Quantity { get; set; }
        public double? TotalPrice { get; set; }
    }
}
