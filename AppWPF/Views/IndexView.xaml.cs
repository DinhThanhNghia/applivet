﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppWPF.Views
{
    /// <summary>
    /// Interaction logic for IndexView.xaml
    /// </summary>
    public partial class IndexView : UserControl
    {
        public IndexView()
        {
            InitializeComponent();
            labelUsername.Content = Application.Current.Properties["UserName"].ToString();
        }

        private void ButtonLogout_Click(object sender, RoutedEventArgs e)
        {
            // Yes/No confirmation box.
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to logout?", "Logout Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Remove token and username from App.Current.Properties.
                Application.Current.Properties["UserName"] = null;

                // Show Login Window.
                Login login = new Login();
                login.Show();

                // Close this Main Window.
                MainWindow mainWindow = (MainWindow)Window.GetWindow(this);
                mainWindow.Close();
            }
        }
    }
}
