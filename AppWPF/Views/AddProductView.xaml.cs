﻿using AppWPF.Models;
using AppWPF.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppWPF.Views
{
    /// <summary>
    /// Interaction logic for AddProductView.xaml
    /// </summary>
    public partial class AddProductView : UserControl
    {
        private readonly ProductService productService = new ProductService();

        public AddProductView()
        {
            InitializeComponent();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            AddProduct();
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            // Yes/No confirmation box.
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to reset?", "Reset Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Clear all input fields.
                Reset();
            }
        }

        private void Reset()
        {
            // Clear all input fields.
            textBoxProductName.Text = "";
            textBoxDetail.Text = "";
            textBoxPrice.Text = "";
            textBoxUnit.Text = "";
            textBoxStatus.Text = "";
            textBlockResponseMessage.Text = "";
        }

        private async void AddProduct()
        {
            string returnMessage = "Validation Message:";
            bool returnFlag = false;

            if (string.IsNullOrEmpty(textBoxProductName.Text))
            {
                returnMessage += Environment.NewLine + "- Product's name must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(textBoxDetail.Text))
            {
                returnMessage += Environment.NewLine + "- Description must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(textBoxUnit.Text))
            {
                returnMessage += Environment.NewLine + "- Unit must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(textBoxPrice.Text))
            {
                returnMessage += Environment.NewLine + "- Price must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(textBoxStatus.Text))
            {
                returnMessage += Environment.NewLine + "- Status must not be empty.";
                returnFlag = true;
            }
            if (returnFlag)
            {
                MessageBox.Show(returnMessage);
            }
            else
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to add this product?", "Save Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    // Get input data from current view.
                    string name = textBoxProductName.Text;
                    string detail = textBoxDetail.Text;
                    double price = double.Parse(textBoxPrice.Text);
                    string unit = textBoxUnit.Text;
                    string status = textBoxStatus.Text;
                    // Create new Product model to push it to the AddProduct function.
                    Product product = new Product()
                    {
                        Name = name,
                        Detail = detail,
                        Price = price,
                        Unit = unit,
                        Status = status,
                        IsDeleted = false
                    };

                    // Function call the API to add Product and get response data.
                    var response = await productService.AddProduct(product);
                    var responseData = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        // Clear all input fields.
                        Reset();

                        // Change response message text to Green.
                        textBlockResponseMessage.Foreground = Brushes.Green;
                    }
                    else
                    {
                        // Change response message text to Red.
                        textBlockResponseMessage.Foreground = Brushes.Red;
                    }

                    // Show the response message from the server.
                    textBlockResponseMessage.Text = responseData;
                }
            }
        }

        private void TextBoxPrice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
