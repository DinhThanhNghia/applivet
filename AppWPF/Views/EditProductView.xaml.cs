﻿using AppWPF.Models;
using AppWPF.Services;
using AppWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppWPF.Views
{
    /// <summary>
    /// Interaction logic for EditProductView.xaml
    /// </summary>
    public partial class EditProductView : UserControl
    {
        private readonly ProductService productService = new ProductService();

        public EditProductView()
        {
            InitializeComponent();
            GetProduct();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            EditProduct();
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            // Yes/No confirmation box.
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to reset?", "Reset Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Clear all input fields.
                Reset();
            }
        }

        private void Reset()
        {
            // Clear all input fields.
            textBoxProductName.Text = "";
            textBoxDetail.Text = "";
            textBoxPrice.Text = "";
            textBoxUnit.Text = "";
            textBoxStatus.Text = "";
            textBlockResponseMessage.Text = "";
        }

        private async void GetProduct()
        {
            // Get data from the App.Current.Properties. These work like global variables.
            int productId = int.Parse(Application.Current.Properties["Id"].ToString());

            // Function call the API to get Product and get response data.
            Product product = await productService.GetProduct(productId);

            // Assign it to the view.   
            textBoxProductName.Text = product.Name;
            textBoxDetail.Text = product.Detail;
            textBoxPrice.Text = product.Price.ToString();
            textBoxStatus.Text = product.Status;
            textBoxUnit.Text = product.Unit;
        }

        private async void EditProduct()
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to edit this product?", "Edit Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Get data from the App.Current.Properties. These work like global variables.
                int id = int.Parse(Application.Current.Properties["Id"].ToString());

                // Get input data from current view.
                string name = textBoxProductName.Text;
                string detail = textBoxDetail.Text;
                double price = double.Parse(textBoxPrice.Text);
                string unit = textBoxUnit.Text;
                string status = textBoxStatus.Text;

                // Create new Product model to push it to the EditProduct function.
                Product product = new Product()
                {
                    Id = id,
                    Name = name,
                    Detail = detail,
                    Price = price,
                    Unit = unit,
                    Status = status,
                    ModifiedDate = DateTime.Now
                };

                // Function call the API to add Product and get response data.
                var response = await productService.EditProduct(product);
                var responseData = response.Content.ReadAsStringAsync().Result;

                if (response.IsSuccessStatusCode)
                {
                    // Remove the productId set in App.Current.Properties.
                    Application.Current.Properties["Id"] = null;

                    // Navigate from this view to another view.
                    MainWindow mainWindow = (MainWindow)Window.GetWindow(this);
                    mainWindow.mainContent.Content = new ProductViewModel();
                }
                else
                {
                    // Change response message text to Red.
                    textBlockResponseMessage.Foreground = Brushes.Red;
                }

                // Show the response message from the server.
                textBlockResponseMessage.Text = responseData;
            }
        }

        private void TextBoxPrice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
