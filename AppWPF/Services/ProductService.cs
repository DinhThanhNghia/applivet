﻿using AppWPF.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AppWPF.Services
{
    public class ProductService
    {
        private readonly CommonService commonServices = new CommonService();
        private readonly string apiControllerUrl = "/Product";

        public async Task<List<Product>> ListProduct()
        {
            // Create API URL and call it to get response without json body (get method).
            HttpResponseMessage response = await commonServices.HttpRequest("GET", apiControllerUrl + "/", null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data and then return.
                return JsonConvert.DeserializeObject<List<Product>>(responseData);
            }

            MessageBox.Show(responseData, "Response Message");

            return null;
        }

        public async Task<Product> GetProduct(int productId)
        {
            // Create API URL and call it to get response without json body (get method).
            HttpResponseMessage response = await commonServices.HttpRequest("GET", apiControllerUrl + "/" + productId, null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data and then return.
                return JsonConvert.DeserializeObject<Product>(responseData);
            }

            MessageBox.Show(responseData, "Response Message");

            return null;
        }

        public async Task<HttpResponseMessage> AddProduct(Product product)
        {
            //var settings = new JsonSerializerSettings { DateFormatString = "yyyy-MM-ddTH:mm:ss.fffZ" };
            // Create a JSON body to send with the request to the API.
            string jsonBody = "{" +
                    "'Name': '" + product.Name + "', " +
                    "'Detail': '" + product.Detail + "', " +
                    "'Price': '" + product.Price + "', " +
                    "'Status': '" + product.Status + "', " +
                    "'IsDeleted': '" + product.IsDeleted + "', " +
                    "'Unit': '" + product.Unit +
                    "'}";

            // Create API URL and call it to get response with json body (post method).
            HttpResponseMessage response = await commonServices.HttpRequest("POST", apiControllerUrl + "/", jsonBody);
            string responseData = response.Content.ReadAsStringAsync().Result;

            MessageBox.Show(responseData, "Response Message");

            return response;
        }

        public async Task<HttpResponseMessage> EditProduct(Product product)
        {
            // Create a JSON body to send with the request to the API.
            string jsonBody = "{" +
                    "'Id': '" + product.Id + "', " +
                    "'Name': '" + product.Name + "', " +
                    "'Detail': '" + product.Detail + "', " +
                    "'Price': '" + product.Price + "', " +
                    "'Status': '" + product.Status + "', " +
                    "'IsDeleted': '" + product.IsDeleted + "', " +
                    "'Unit': '" + product.Unit +
                    "'}";

            // Create API URL and call it to get response with json body (post method).
            HttpResponseMessage response = await commonServices.HttpRequest("PUT", apiControllerUrl + "/", jsonBody);
            string responseData = response.Content.ReadAsStringAsync().Result;

            MessageBox.Show(responseData, "Response Message");

            return response;
        }

        public async Task<HttpResponseMessage> DeleteProduct(int productId)
        {
            // Create API URL and call it to get response without json body (delete method).
            HttpResponseMessage response = await commonServices.HttpRequest("DELETE", apiControllerUrl + "/" + productId, null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            MessageBox.Show(responseData, "Response Message");

            return response;
        }
    }
}
