﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AppWPF.Services
{
    public partial class CommonService
    {
        public string GetApiUrl()
        {
            // Get API URL from the App.config file.
            return ConfigurationManager.AppSettings["AppAPI"];
        }

        public string GetToken()
        {
            // Get token data from the App.Current.Properties. This works like a global variable.
            return Application.Current.Properties["token"].ToString();
        }

        public async Task<HttpResponseMessage> HttpRequest(string method, string apiControllerUrl, string jsonBody)
        {
            //get api url from app.config
            string apiUrl = GetApiUrl();

            HttpClient httpClient = new HttpClient();

            HttpResponseMessage response = null;

            // Check method and call API accordingly.
            if (string.Equals(method, "GET", StringComparison.OrdinalIgnoreCase))
            {
                response = httpClient.GetAsync(apiUrl + apiControllerUrl).Result;
            }
            else if (string.Equals(method, "POST", StringComparison.OrdinalIgnoreCase))
            {
                response = await httpClient.PostAsync(apiUrl + apiControllerUrl,
                    new StringContent(jsonBody, Encoding.UTF8, "application/json"));
            }
            else if (string.Equals(method, "PUT", StringComparison.OrdinalIgnoreCase))
            {
                response = await httpClient.PutAsync(apiUrl + apiControllerUrl,
                    new StringContent(jsonBody, Encoding.UTF8, "application/json"));
            }
            else if (string.Equals(method, "DELETE", StringComparison.OrdinalIgnoreCase))
            {
                response = httpClient.DeleteAsync(apiUrl + apiControllerUrl).Result;
            }

            return response;
        }
    }
}