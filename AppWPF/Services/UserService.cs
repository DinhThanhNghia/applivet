﻿using System.Net.Http;
using System.Threading.Tasks;

namespace AppWPF.Services
{
    public class UserService
    {
        private readonly CommonService commonServices = new CommonService();
        private readonly string apiControllerUrl = "/User";

        public async Task<HttpResponseMessage> Login(string username, string password)
        {
            // Create a JSON body to send with the request to the API.
            string jsonBody = "{'Username': '" + username + "', 'Password': '" + password + "'}";

            // Create API URL and call it to get response with json body (pass "LOGIN" string as method to check).
            return await commonServices.HttpRequest("POST", apiControllerUrl + "/", jsonBody);
        }
    }
}