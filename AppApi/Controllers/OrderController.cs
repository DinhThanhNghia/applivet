﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppApi.Models;
using AppApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<IActionResult> ListOrder()
        {
            var orderList = await _orderService.ListOrder();
            // Return status success with the Product List data to the requested client.
            return StatusCode(200, orderList);
        }

        [HttpGet]
        [Route("GetAllPaging/_page={pageIndex}&_limit={pageSize}")]
        public async Task<IActionResult> ListOrder(int pageSize, int pageIndex)
        {
            var orderList = await _orderService.ListOrderPaging(pageSize, pageIndex);
            // Return status success with the Product List data to the requested client.
            return StatusCode(200, orderList);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrderList([FromBody] List<Order> orders)
        {
            // Check if all rows of the order list is added successfully or not.
            int orderListCount = 0;
            int successAddCount = 0;
            int result = 0;
            // Loop to add order rows.
            foreach (Order order in orders)
            {
                orderListCount++;
                // Add order row.
                result = await _orderService.AddOrder(order);
                if (result != 0)
                {
                    successAddCount++;
                }
            }
            if (orderListCount == successAddCount)
            {
                // Return OK if all rows added sucessfully.
                return StatusCode(200, "Order List added sucessfully.");
            }
            return Unauthorized();
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            var order = await _orderService.DeleteOrder(id);
            if (order != 0)
            {
                //return status code ok
                return StatusCode(200, "Order deleted successfully");
            }
            return Unauthorized();
        }
    }
}