﻿using AppApi.Models;
using AppApi.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace AppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProductController(IProductService productService, IHostingEnvironment hostingEnvironment)
        {
            _productService = productService;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> ListProduct()
        {
            //Get product from service
            var product = await _productService.ListProduct();
            //Return ok status
            return StatusCode(200, product);
        }

        [HttpGet]
        [Route("GetAllPaging/_page={page}&_limit={pageSize}")]
        public async Task<IActionResult> ListProduct(int pageSize, int page)
        {
            //Get product from service
            var product = await _productService.ListProductPaging(pageSize, page);
            //Return ok status
            return StatusCode(200, product);
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetProduct(int id)
        {
            //Get product by Id
            Product product = await _productService.GetProduct(id);
            //return ok status
            return StatusCode(200, product);
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct([FromBody] Product product)
        {
            //Message for validation
            string returnMessage = "Validation message: ";
            // Set return flag to check if validation is success or fail.
            bool returnFlag = false;
            // Validate Product input fields sent from the client.
            if (string.IsNullOrEmpty(product.Name))
            {
                returnMessage += Environment.NewLine + "- Product field is required";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(product.Detail))
            {
                returnMessage += Environment.NewLine + "- Detail field is required";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(product.Unit))
            {
                returnMessage += Environment.NewLine + "- Unit field is required";
                returnFlag = true;
            }
            // If even one of them failed the validation, return error message.
            if (returnFlag)
            {
                return StatusCode(412, returnMessage);
            }
            // Else, add the Product to the database.
            var addProduct = await _productService.AddProduct(product);
            if (addProduct != 0)
            {
                // Return OK if added sucessfully.
                return StatusCode(200, "New Product added sucessfully.");
            }
            return Unauthorized();
        }

        [HttpPut]
        public async Task<IActionResult> EditProduct([FromBody] Product product)
        {
            //Message for validation
            string returnMessage = "Validation message: ";
            // Set return flag to check if validation is success or fail.
            bool returnFlag = false;
            // Validate Product input fields sent from the client.
            if (string.IsNullOrEmpty(product.Name))
            {
                returnMessage += Environment.NewLine + "- Product field is required";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(product.Detail))
            {
                returnMessage += Environment.NewLine + "- Detail field is required";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(product.Unit))
            {
                returnMessage += Environment.NewLine + "- Unit field is required";
                returnFlag = true;
            }
            // If even one of them failed the validation, return error message.
            if (returnFlag)
            {
                return StatusCode(412, returnMessage);
            }
            // Else, add the Product to the database.
            var addProduct = await _productService.EditProduct(product);
            if (addProduct != 0)
            {
                // Return OK if added sucessfully.
                return StatusCode(200, "Product updated sucessfully.");
            }
            return Unauthorized();
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var product = await _productService.DeleteProduct(id);
            if (product != 0)
            {
                //return status code ok
                return StatusCode(200, "Product deleted successfully");
            }
            return Unauthorized();
        }
    }
}