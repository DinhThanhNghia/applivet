﻿using AppApi.Models;
using AppApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace AppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;

        public UserController(IUserService userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] User model)
        {
            //Check login
            User user = await _userService.CheckLogin(model.UserName, model.Password);

            if (user != null)
            {
                return Ok(new
                {
                    user.Id
                });
            }
            return Unauthorized("User name or password is incorrect!");
        }

        private IActionResult Unauthorized(string v)
        {
            throw new NotImplementedException();
        }
    }
}