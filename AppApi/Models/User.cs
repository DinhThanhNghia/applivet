﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Models
{
    [Table("Users")]
    public class User
    {
        public User()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(255)]
        [Required]
        public string UserName { get; set; }

        [StringLength(255)]
        [Required]
        public string Password { get; set; }

        public virtual ICollection<Order> Orders { set; get; }
    }
}
