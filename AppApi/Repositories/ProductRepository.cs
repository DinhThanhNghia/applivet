﻿using AppApi.Models;
using AppApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly AppDbContext _context;

        public ProductRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddProduct(Product product)
        {
            int result = 0;
            if (_context != null)
            {
                //Add product
                product.CreatedDate = DateTime.Now;
                await _context.Products.AddAsync(product);

                //Commit
                await _context.SaveChangesAsync();

                //Get Id to return
                result = product.Id;
            }
            return result;
        }

        public async Task<int> DeleteProduct(int? id)
        {
            int result = 0;
            if (_context != null)
            {
                //Find product by id
                var product = await _context.Products.FirstOrDefaultAsync(x => x.Id == id);

                if (product != null)
                {
                    //Delete this product
                    //_context.Products.Remove(product);
                    product.IsDeleted = true;

                    //Commit
                    result = await _context.SaveChangesAsync();
                }
            }
            return result;
        }

        public async Task<int> EditProduct(Product product)
        {
            int result = 0;
            if (_context != null)
            {
                //Edit product
                _context.Products.Update(product);

                //Commit
                result = await _context.SaveChangesAsync();
            }
            return result;
        }

        public async Task<Product> GetProduct(int? id)
        {
            if (_context != null)
            {
                //select by id
                return await (from p in _context.Products
                             where p.Id == id
                             select new Product
                             {
                                 Id = p.Id,
                                 Name = p.Name,
                                 Detail = p.Detail,
                                 UrlImage = p.UrlImage,
                                 Price = p.Price,
                                 Unit = p.Unit,
                                 Quantity = p.Quantity,
                                 CreatedDate = p.CreatedDate,
                                 IsDeleted = p.IsDeleted
                             }).FirstOrDefaultAsync();
            }
            return null;
        }

        public async Task<List<Product>> ListProduct()
        {
            if (_context != null)
            {
                //select all product
                return await (from p in _context.Products
                              where p.IsDeleted == false
                              orderby p.Id descending
                              select new Product
                              {
                                  Id = p.Id,
                                  Name = p.Name,
                                  Detail = p.Detail,
                                  UrlImage = p.UrlImage,
                                  Price = p.Price,
                                  Unit = p.Unit,
                                  Quantity = p.Quantity,
                                  CreatedDate = p.CreatedDate,
                                  IsDeleted = p.IsDeleted
                              }).ToListAsync();
            }
            return null;
        }

        public async Task<List<Product>> ListProductPaging(int pageSize, int pageIndex)
        {
            if (_context != null)
            {
                //select all product
                return await (from p in _context.Products
                             where p.IsDeleted == false
                             orderby p.Id descending
                             select new Product
                             {
                                 Id = p.Id,
                                 Name = p.Name,
                                 Detail = p.Detail,
                                 UrlImage = p.UrlImage,
                                 Price = p.Price,
                                 Quantity = p.Quantity,
                                 Unit = p.Unit,
                                 CreatedDate = p.CreatedDate,
                                 IsDeleted = p.IsDeleted
                             }).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToListAsync();
            }
            return null;
        }
    }
}
