﻿using AppApi.Models;
using AppApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly AppDbContext _context;

        public OrderRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddOrder(Order order)
        {
            int result = 0;
            if (_context != null)
            {
                order.CreatedDate = DateTime.Now;
                // Add that Order.
                await _context.Orders.AddAsync(order);

                // Commit the transaction.
                await _context.SaveChangesAsync();

                result = order.Id;
            }
            return result;
        }

        public async Task<int> DeleteOrder(int? id)
        {
            int result = 0;
            if (_context != null)
            {
                //Find product by id
                var order = await _context.Orders.FirstOrDefaultAsync(x => x.Id == id);

                if (order != null)
                {
                    //Delete this product
                    order.IsDeleted = true;

                    //Commit
                    result = await _context.SaveChangesAsync();
                }
            }
            return result;
        }

        public async Task<List<Order>> ListOrder()
        {
            if (_context != null)
            {
                // Select all Products from Product table in the db.
                return await (from o in _context.Orders
                              orderby o.Id descending
                              where o.IsDeleted == false
                              select new Order
                              {
                                  Id = o.Id,
                                  User = o.User,
                                  ProductId = o.ProductId,
                                  UserId = o.UserId,
                                  CreatedDate = o.CreatedDate,
                                  Product = o.Product,
                                  Quantity = o.Quantity
                              }).ToListAsync();
            }
            return null;
        }

        public async Task<List<Order>> ListOrderPaging(int pageSize, int pageIndex)
        {
            if (_context != null)
            {
                // Select all Products from Product table in the db.
                return await (from o in _context.Orders
                             orderby o.Id descending
                             where o.IsDeleted == false
                             select new Order
                             {
                                 Id = o.Id,
                                 User = o.User,
                                 ProductId = o.ProductId,
                                 UserId = o.UserId,
                                 CreatedDate = o.CreatedDate,
                                 Product = o.Product,
                                 Quantity = o.Quantity
                             }).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToListAsync();
            }
            return null;
        }
    }
}
