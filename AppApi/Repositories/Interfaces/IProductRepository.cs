﻿using AppApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppApi.Repositories.Interfaces
{
    public interface IProductRepository
    {
        Task<int> AddProduct(Product product);

        Task<int> EditProduct(Product product);

        Task<int> DeleteProduct(int? id);

        Task<Product> GetProduct(int? id);

        Task<List<Product>> ListProduct();

        Task<List<Product>> ListProductPaging(int pageSize, int pageIndex);
    }
}