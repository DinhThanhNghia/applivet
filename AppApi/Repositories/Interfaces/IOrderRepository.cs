﻿using AppApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppApi.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        Task<int> AddOrder(Order order);

        Task<List<Order>> ListOrder();

        Task<List<Order>> ListOrderPaging(int pageSize, int pageIndex);

        Task<int> DeleteOrder(int? id);

    }
}