﻿using AppApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<User> CheckLogin(string userName, string password);

        //Task<int> AddToken(User user);

        //Task<User> CheckTokenExist(string user);

        //Task<int> CheckBearerToken(string bearerToken);
    }
}
