﻿using AppApi.Common;
using AppApi.Models;
using AppApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _context;

        public UserRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<User> CheckLogin(string userName, string password)
        {
            if (_context != null)
            {
                return await (from u in _context.Users
                              where u.UserName == userName
                              && u.Password == Encryptor.MD5Hash(password)
                              select new User
                              {
                                  Id = u.Id,
                                  UserName = u.UserName,
                                  Password = u.Password
                              }).FirstOrDefaultAsync();
            }
            return null;
        }
    }
}
