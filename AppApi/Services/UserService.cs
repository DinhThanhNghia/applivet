﻿using AppApi.Models;
using AppApi.Repositories.Interfaces;
using AppApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public Task<User> CheckLogin(string userName, string password)
        {
            return _userRepository.CheckLogin(userName, password);
        }
    }
}
