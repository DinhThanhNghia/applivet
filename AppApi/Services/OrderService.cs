﻿using AppApi.Models;
using AppApi.Repositories.Interfaces;
using AppApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Task<int> AddOrder(Order order)
        {
            return _orderRepository.AddOrder(order);
        }

        public Task<int> DeleteOrder(int? id)
        {
            return _orderRepository.DeleteOrder(id);
        }

        public Task<List<Order>> ListOrder()
        {
            return _orderRepository.ListOrder();
        }

        public Task<List<Order>> ListOrderPaging(int pageSize, int pageIndex)
        {
            return _orderRepository.ListOrderPaging(pageSize, pageIndex);
        }
    }
}
