﻿using AppApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppApi.Services.Interfaces
{
    public interface IProductService
    {
        Task<int> AddProduct(Product product);

        Task<int> EditProduct(Product product);

        Task<int> DeleteProduct(int? id);

        Task<Product> GetProduct(int? id);

        Task<List<Product>> ListProduct();

        Task<List<Product>> ListProductPaging(int pageSize, int page);
    }
}