﻿using AppApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.Services.Interfaces
{
    public interface IUserService
    {
        Task<User> CheckLogin(string userName, string password);
    }
}
