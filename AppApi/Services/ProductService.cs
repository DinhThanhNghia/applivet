﻿using AppApi.Models;
using AppApi.Repositories.Interfaces;
using AppApi.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppApi.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Task<int> AddProduct(Product product)
        {
            return _productRepository.AddProduct(product);
        }

        public Task<int> DeleteProduct(int? id)
        {
            return _productRepository.DeleteProduct(id);
        }

        public Task<int> EditProduct(Product product)
        {
            return _productRepository.EditProduct(product);
        }

        public Task<Product> GetProduct(int? id)
        {
            return _productRepository.GetProduct(id);
        }

        public Task<List<Product>> ListProduct()
        {
            return _productRepository.ListProduct();
        }

        public Task<List<Product>> ListProductPaging(int pageSize, int page)
        {
            return _productRepository.ListProductPaging(pageSize, page);
        }
    }
}